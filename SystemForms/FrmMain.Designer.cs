﻿namespace SystemForms
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.flp = new System.Windows.Forms.FlowLayoutPanel();
            this.btCheck = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1037, 567);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(109, 67);
            this.button1.TabIndex = 0;
            this.button1.Text = "串口从站配置";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 567);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(109, 67);
            this.button2.TabIndex = 1;
            this.button2.Text = "区域配置";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(1189, 673);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(109, 91);
            this.button4.TabIndex = 3;
            this.button4.Text = "退出";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // flp
            // 
            this.flp.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.flp.Location = new System.Drawing.Point(12, 131);
            this.flp.Name = "flp";
            this.flp.Size = new System.Drawing.Size(1312, 430);
            this.flp.TabIndex = 4;
            // 
            // btCheck
            // 
            this.btCheck.Location = new System.Drawing.Point(1037, 40);
            this.btCheck.Name = "btCheck";
            this.btCheck.Size = new System.Drawing.Size(110, 64);
            this.btCheck.TabIndex = 5;
            this.btCheck.Text = "实时监测";
            this.btCheck.UseVisualStyleBackColor = true;
            this.btCheck.Click += new System.EventHandler(this.button3_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(143, 567);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(109, 67);
            this.button3.TabIndex = 6;
            this.button3.Text = "刷新区域";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(29, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(833, 72);
            this.label1.TabIndex = 7;
            this.label1.Text = "车间工位温湿度监控系统";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1329, 776);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btCheck);
            this.Controls.Add(this.flp);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "FrmMain";
            this.Text = "FrmMain";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.FlowLayoutPanel flp;
        private System.Windows.Forms.Button btCheck;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
    }
}