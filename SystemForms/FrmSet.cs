﻿using Microsoft.Win32;
using Modbus.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using static System.Windows.Forms.AxHost;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TextBox;

namespace SystemForms
{
    public partial class FrmSet : Form
    {
        public FrmSet()
        {
            InitializeComponent();
        }

        private int[] serialBuad = { 300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 38400, 56000, 57600, 115200, 128000, 230400, 25600 };
        private int[] serialDatabit = { 5, 6, 7, 8 };
        public static double[] serialStopbits = { 1, 1.5, 2 };
        public static string[] serialParity = { "None Parity", "Odd Parity", "Even Parity" };
        private string[] option = { "3", "4" };

        public static string CommPath = Path.Combine(FrmMain.filePath, "set.txt");//串口信息保存目录
        public static string SlavePath = Path.Combine(FrmMain.filePath, "XSlaves.xml");//从站信息保存目录


        XmlDocument Xslaves = new XmlDocument();
        
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void FrmSet_Load(object sender, EventArgs e)
        {


            
            
            LoadTxt();//加载串口选项
            //查询是否有xml
            if (File.Exists(SlavePath))
            {
                Xslaves.Load(SlavePath);
            }
            else
            {
                
                Xslaves.AppendChild(Xslaves.CreateXmlDeclaration("1.0", "UTF-8", null));
                
                XmlElement xmlElement= Xslaves.CreateElement("Slaves");
                Xslaves.AppendChild(xmlElement);  

            }
            LoadSlaves();//加载从站列表
           
        }
        

        //加载从站表格
        private void LoadSlaves()
        {

            if (FrmMain.slavesList.Count>0)
            {
                listBox1.Items.Clear();
                foreach (SlaveInfo slave in FrmMain.slavesList)
                {
                    listBox1.Items.Add($"{slave.SlaveId}        {slave.FunctionCode}        {slave.StartAddrs}      {slave.Count}");

                }
               


            }
        }

       



        //加载串口选项
        private void LoadTxt()
        {
            SerialPort port = new SerialPort();
            RegistryKey key = Registry.LocalMachine.OpenSubKey(@"Hardware\DeviceMap\SerialComm");
            if (key != null)
            {
                string[] strings = key.GetValueNames();
                foreach (string s in strings)
                {
                    string Values = (string)key.GetValue(s);
                    cb_serialPort.Items.Add(Values);

                }
                cb_serialPort.SelectedIndex = 0;
            }

            foreach (int s in serialBuad)
            { 
            cb_Baud.Items.Add(s);
            }
            cb_Baud.SelectedIndex = 11;

            foreach (int s in serialDatabit)
            { 
            cb_Databits.Items.Add(s);
            }
            cb_Databits.SelectedIndex = 3;

            foreach (double d in serialStopbits)
            { 
            cb_StopBits.Items.Add(d);
            }
            cb_StopBits.SelectedIndex = 0;

            foreach (string s in serialParity)
            { 
            cb_Parity.Items.Add(s);
            }
            cb_Parity.SelectedIndex = 0;

            foreach (string s in option)
            { 
            cbFunctionCodes.Items.Add(s);
            }
            cbFunctionCodes.SelectedIndex = 0;
        }
        //保存串口信息
        private void button1_Click(object sender, EventArgs e)
        {
            string portName = cb_serialPort.Text.Trim();
            string baudRate = cb_Baud.Text.Trim();
            string dataBits = cb_Databits.Text.Trim();
            string stopBits = cb_StopBits.Text.Trim();
            string parity = cb_Parity.Text.Trim();

            StreamWriter sw;

            if (!File.Exists(CommPath))
            {
               sw= File.CreateText(CommPath);
                sw.Close();
            }
           
                string conSet = $"PortName:{portName};Baudrate:{baudRate};DataBits:{dataBits};StopBits:{stopBits};Parity:{parity}";
                File.WriteAllText(CommPath, conSet);
            
           
                
            


           
            MessageBox.Show("通信设置保存成功");
            try
            {
                FrmMain.serialPort.Close();
            }
            catch (Exception  ee)
            {

                MessageBox.Show(ee.ToString());
            }
            
            FrmMain.Isopen=false;
            FrmMain.LoadConn();


        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (txtSlaveId.Text.Trim()==""||txtStartAddr.Text.Trim()==""||txtCount.Text.Trim()==""||txtSlaveId.Text.Trim()=="")
            {
                
                    MessageBox.Show("有空项，请输入");
                    return;
                
            }
            try
            {
                byte salveId = byte.Parse(txtSlaveId.Text);
                byte funCode = byte.Parse(cbFunctionCodes.Text);
                ushort startAddr = ushort.Parse(txtStartAddr.Text);
                ushort count = ushort.Parse(txtCount.Text);

                var root = Xslaves.DocumentElement;
                //

                SlaveInfo slaveInfo = FrmMain.slavesList.Find(s => s.SlaveId == salveId);
                if (slaveInfo == null)
                {
                    slaveInfo = new SlaveInfo()
                    {
                        SlaveId = salveId,
                        FunctionCode = funCode,
                        StartAddrs = startAddr,
                        Count = count,
                    };
                    FrmMain.slavesList.Add(slaveInfo);
                    XmlElement xele = Xslaves.CreateElement("SlaveInfo");
                    root.AppendChild(xele);

                    XmlElement xmlElement = Xslaves.CreateElement("SlaveId");
                    xmlElement.InnerText = salveId.ToString();
                    xele.AppendChild(xmlElement);

                    XmlElement xmlElement2 = Xslaves.CreateElement("FunctionCode");
                    xmlElement2.InnerText = funCode.ToString();
                    xele.AppendChild(xmlElement2);

                    XmlElement xmlElement3 = Xslaves.CreateElement("StartAddrs");
                    xmlElement3.InnerText = startAddr.ToString();
                    xele.AppendChild(xmlElement3);

                    XmlElement xmlElement4 = Xslaves.CreateElement("Count");
                    xmlElement4.InnerText = count.ToString();
                    xele.AppendChild(xmlElement4);



                }
                else
                {
                    slaveInfo.SlaveId = salveId;
                    slaveInfo.FunctionCode = funCode;
                    slaveInfo.StartAddrs = startAddr;
                    slaveInfo.Count = count;


                }
                Xslaves.Save(SlavePath);
            }
            catch (Exception ee)
            {

                MessageBox.Show(ee.ToString()); 
            }
           

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FrmMain.LoadSlaveList();
            LoadSlaves();

        }
        private int Seid;
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItems != null)
            {
                if (listBox1.SelectedItem == null)
                {
                    return;
                }
                string st = listBox1.SelectedItem.ToString();
                string[] arr = st.Split(' ').Where(str => str != "").ToArray();
                Seid = int.Parse(arr[0]);
                txtSlaveId.Text = arr[0];
                cbFunctionCodes.Text = arr[1];
                txtStartAddr.Text = arr[2];
                txtCount.Text = arr[3];

            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (Seid>0)
            {
                if (MessageBox.Show("删除从站", $"你确定要删除{Seid}从站?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    SlaveInfo slaveInfo = FrmMain.slavesList.Find(s => s.SlaveId == Seid);
                    if (slaveInfo != null)
                    {

                        var root = Xslaves.DocumentElement;
                        var node = root.SelectSingleNode($"SlaveInfo[SlaveId={Seid}]");
                        if (node != null)
                        {
                            FrmMain.slavesList.Remove(slaveInfo);
                            root.RemoveChild(node);
                            MessageBox.Show($"删除{Seid}从站成功！");
                            Xslaves.Save(SlavePath);
                        }

                    }
                }
                
                   
                
            }
        }
    }
}
