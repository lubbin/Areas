﻿using Modbus.Device;
using Modbus.Extensions.Enron;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using static System.Windows.Forms.AxHost;
using SystemForms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Button;
using System.Threading;
using System.Runtime.ConstrainedExecution;

namespace SystemForms
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }
        public static string filePath = Application.StartupPath + "/ConfigFiles";//目录文件夹
        public static List<string> connectionSets = new List<string>();//暂存通信信息
        public static bool Isopen = false;//连接状态
        public static SerialPort serialPort = null;//串口
        public static ModbusSerialMaster master = null;//主站

        public static List<SlaveInfo> slavesList = new List<SlaveInfo>();//从站列表
        public static List<AreaInfo> areasList = new List<AreaInfo>();//区域列表

        Dictionary<int, AreaData> dicDatas = new Dictionary<int, AreaData>();//实时信息，字典

        public static bool Ischeck = false;



        private void FrmMain_Load(object sender, EventArgs e)//窗体加载
        {
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            LoadConn();//加载串口信息

            //加载从站列表
            LoadSlaveList();

            //加载区域列表
            LoadAreaList();
            //加载区域窗体
            LoadC();
            //加载区域窗体信息
            LoadList1();


        }

        private void LoadList1()
        {
            try
            {
                Task.Run(async () =>
                {


                    foreach (var slave in slavesList)
                    {
                        ushort[] Udatas = null;

                        if (slave.FunctionCode == 4)
                        {
                            Udatas = await master.ReadInputRegistersAsync(slave.SlaveId, slave.StartAddrs, slave.Count);
                        }
                        if (slave.FunctionCode == 3)
                        {
                            Udatas = await master.ReadHoldingRegistersAsync(slave.SlaveId, slave.StartAddrs, slave.Count);
                        }
                        if (Udatas.Length > 0)
                        {
                            var areas = areasList.Find(a => a.SlaveId == slave.SlaveId);
                            if (areas != null)
                            {
                                ushort wenduAddr = areas.Wendu;
                                ushort ShiduAddr = areas.Shidu;
                                AreaData areadata = new AreaData();
                                decimal Wdata = Udatas[wenduAddr];
                                decimal Sdata = Udatas[ShiduAddr];
                                areadata.Wendu = decimal.Parse(((double)Wdata / Math.Pow(10, 1)).ToString("0.0"));
                                areadata.Shdu = decimal.Parse(((double)Sdata / Math.Pow(10, 1)).ToString("0.0"));
                                areadata.Name = areas.AreaName;



                                if (dicDatas.ContainsKey(areas.AreaId))
                                {
                                    dicDatas[areas.AreaId] = areadata;
                                }
                                else
                                {
                                    dicDatas.Add(areas.AreaId, areadata);

                                }
                            }



                            //获取一次区域地址信息，存到字典

                        }
                    }



                    this.Invoke(new Action(() =>
                    {
                        foreach (Control c in flp.Controls)
                        {
                            if (c is UserControl1)
                            {
                                UserControl1 User = (UserControl1)c;

                                int Num = User.ID;
                                try
                                {
                                    AreaData Datas = dicDatas[Num];
                                    User.ID = Num;
                                    User.Wendu = Datas.Wendu.ToString();
                                    User.Shidu = Datas.Shdu.ToString();
                                    User.Name = Datas.Name.ToString();
                                }
                                catch (Exception ee)
                                {

                                    MessageBox.Show("区域配置信息错误请检查");
                                }









                            }

                        }



                    }));


                });
            }
            catch (Exception ee)
            {

                MessageBox.Show("串口以更改");
            }

            



        }


        public  void LoadC()
        {

            foreach (var info in areasList)
            {



                UserControl1 Ud = new UserControl1();
                Ud.ID = info.AreaId;


                this.flp.Controls.Add(Ud);

            }
        }







        //加载串口信息
        public static void LoadConn()
        {
            string path = FrmSet.CommPath;
            if (File.Exists(path))
            {
                string connSet = File.ReadAllText(path);
                if (!string.IsNullOrEmpty(connSet))
                {
                    string[] List = connSet.Split(';');
                    connectionSets.Clear();
                    List.ToList().ForEach(x => connectionSets.Add(x.Split(':')[1]));
                    CreateCoon();
                }
            }
            else
            { MessageBox.Show("请配置串口"); }
        }

        //连接串口
        public static void CreateCoon()
        {

            var sets = connectionSets;//通信设置

            serialPort = new SerialPort();
            serialPort.PortName = sets[0];
            serialPort.BaudRate = int.Parse(sets[1]);
            serialPort.DataBits = int.Parse(sets[2]);
            if (sets[3].ToString() == "1")
            {
                serialPort.StopBits = StopBits.One;

            }
            else if (sets[3].ToString() == "2") { serialPort.StopBits = StopBits.Two; }
            else if (sets[3].ToString() == "1.5") { serialPort.StopBits = StopBits.OnePointFive; }

            if (sets[4].ToString() == "None Parity") { serialPort.Parity = Parity.None; }
            if (sets[4].ToString() == "Odd Parity") { serialPort.Parity = Parity.Odd; }
            if (sets[4].ToString() == "Even Parity") { serialPort.Parity = Parity.Even; }


            if (serialPort != null)
            {
                try
                {
                    serialPort.Open();
                    if (serialPort.IsOpen)
                    {
                        Isopen = true;
                        master = ModbusSerialMaster.CreateRtu(serialPort);


                    }
                }
                catch (Exception ee)
                {

                    MessageBox.Show(ee.ToString());
                }
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmSet frmSet = new FrmSet();
            frmSet.ShowDialog();
        }

        //加载从站
        public static void LoadSlaveList()
        {
            slavesList.Clear();
            XmlDocument doc = new XmlDocument();
            string path = FrmSet.SlavePath;
            if (File.Exists(path))
            {   //读取XML从站
                doc.Load(path);
                XmlElement root = doc.DocumentElement;
                if (root != null)
                {
                    foreach (XmlNode node in root)
                    {
                        SlaveInfo slaveInfo = new SlaveInfo();
                        slaveInfo.SlaveId = byte.Parse(node.SelectSingleNode("SlaveId").InnerText);
                        slaveInfo.FunctionCode = byte.Parse(node.SelectSingleNode("FunctionCode").InnerText);
                        slaveInfo.StartAddrs = ushort.Parse(node.SelectSingleNode("StartAddrs").InnerText);
                        slaveInfo.Count = ushort.Parse(node.SelectSingleNode("Count").InnerText);
                        slavesList.Add(slaveInfo);

                    }
                }

            }
        }

        //加载区域
        public static void LoadAreaList()
        {
            areasList.Clear();
            XmlDocument doc = new XmlDocument();
            if (File.Exists(FrmArea.AreaPath))
            {
                doc.Load(FrmArea.AreaPath);

                XmlElement root = doc.DocumentElement;
                if (root != null)
                {
                    foreach (XmlNode node in root)
                    {
                        AreaInfo areaInfo = new AreaInfo();
                        areaInfo.AreaId = int.Parse(node.SelectSingleNode("AreaId").InnerText);
                        areaInfo.AreaName = node.SelectSingleNode("AreaName").InnerText.ToString();
                        areaInfo.SlaveId = byte.Parse(node.SelectSingleNode("SlaveId").InnerText);
                        areaInfo.State = ushort.Parse(node.SelectSingleNode("State").InnerText);
                        areaInfo.Wendu = ushort.Parse(node.SelectSingleNode("C").InnerText);
                        areaInfo.Shidu = ushort.Parse(node.SelectSingleNode("H").InnerText);
                        areasList.Add(areaInfo);
                    }
                }


            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmArea area = new FrmArea();
            area.ShowDialog();
        }

        Thread task = null;
        private void button3_Click(object sender, EventArgs e)
        {
            if (Isopen == false)
            {
                MessageBox.Show("请先连接串口");
                return;
            }
            if (flp.Controls.Count==0)
            {
                MessageBox.Show("请先配置区域信息");
                return;
            }
           
            if (Ischeck == true)
            {
                Ischeck = false;
                btCheck.Text = "实时监测";
                task.Abort();

                return;
                
            }
            if (Ischeck == false)
            {
                Ischeck = true;
                btCheck.Text = "关闭实时监测";
                task = new Thread(() =>
                {
                    while (Ischeck == true)
                    {
                        LoadList1();

                    }
                });

                task.Start();








                return;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Application.ExitThread();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            flp.Controls.Clear();
            
            LoadC();
            LoadList1();
        }
    }
}
