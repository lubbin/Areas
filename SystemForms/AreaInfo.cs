﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemForms
{
    public class AreaInfo
    {
        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public byte SlaveId { get; set; }
        public ushort State { get; set; }

        public ushort Wendu { get; set; }
        public ushort Shidu { get; set; }
    }
}
