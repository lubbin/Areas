﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemForms
{
    public class SlaveInfo
    {
        public byte SlaveId { get; set; }
        public byte FunctionCode { get; set; }
        public ushort StartAddrs { get; set; }
        public ushort Count { get; set; }
    }
}
