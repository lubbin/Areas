﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace SystemForms
{
    public partial class FrmArea : Form
    {
        public FrmArea()
        {
            InitializeComponent();
        }
        public static string AreaPath = Path.Combine(FrmMain.filePath, "XArea.xml");//区域保存目录
        public static XmlDocument XArea = new XmlDocument();//区域xml


        private void FrmArea_Load(object sender, EventArgs e)
        {
            //加载从站
            LoadSlaves();
            //加载配置，有没有区域XML文件
            if (File.Exists(AreaPath))
            {
                XArea.Load(AreaPath);
            }
            else
            {
                CreaNewXml(XArea, "Areas");

            }
            LoadArea(); //加载区域表格
        }

        //加载从站列表
        private void LoadSlaves()
        {
            cb_Slaves.DataSource=null;
            if (FrmMain.slavesList.Count > 0)
            {
                cb_Slaves.DisplayMember = "SlaveId";
                cb_Slaves.DataSource = FrmMain.slavesList;
                cb_Slaves.SelectedIndex = 0;


            }
        }

        //加载区域表格
        private void LoadArea()
        {
            if (FrmMain.areasList.Count>0)
            {
                listBox1.Items.Clear();
                foreach (AreaInfo areaInfo in FrmMain.areasList)
                {
                    listBox1.Items.Add($"{areaInfo.AreaId}     {areaInfo.SlaveId}      {areaInfo.AreaName}        {areaInfo.Wendu}          {areaInfo.Shidu}");

                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmMain.LoadAreaList();
            LoadArea();
        }
        //保存区域信息到Xml
        private void button1_Click(object sender, EventArgs e)
        {
            if (txtAreaId.Text.Trim() == ""||txtAreaName.Text.Trim() == ""||txtShidu.Text.Trim() == ""||txtWendu.Text.Trim()==""||cb_Slaves.Text.Trim()=="")
            {
                MessageBox.Show("有空项，请输入");
                return;
            }
            try
            {
                int AreaId = int.Parse(txtAreaId.Text.Trim());
                string Name = txtAreaName.Text.Trim();
                byte slaveId = byte.Parse(cb_Slaves.Text.Trim());
                ushort WenduAddr = ushort.Parse(txtWendu.Text.Trim());
                ushort ShiduAddr = ushort.Parse(txtShidu.Text.Trim());
                ushort State = ushort.Parse("1");

                var root = XArea.DocumentElement;//获取根节点

                AreaInfo areaInfo = FrmMain.areasList.Find(p => p.AreaId == AreaId);
                if (areaInfo == null)
                {
                    areaInfo = new AreaInfo()
                    {
                        AreaId = AreaId,
                        AreaName = Name,
                        SlaveId = slaveId,
                        Wendu = WenduAddr,
                        Shidu = ShiduAddr
                    };
                    FrmMain.areasList.Add(areaInfo);
                    XmlElement areaEle = Addelement(XArea, "Area", root);
                    AddElement(XArea, "C", areaEle, WenduAddr.ToString());
                    AddElement(XArea, "H", areaEle, ShiduAddr.ToString());


                    AddElement(XArea, "AreaId", areaEle, AreaId.ToString());
                    AddElement(XArea, "AreaName", areaEle, Name.ToString());
                    AddElement(XArea, "SlaveId", areaEle, slaveId.ToString());


                    AddElement(XArea, "State", areaEle, State.ToString());



                }
                else
                {

                    areaInfo.SlaveId = slaveId;
                    areaInfo.AreaName = Name;
                    areaInfo.SlaveId = slaveId;
                    areaInfo.Wendu = WenduAddr;
                    areaInfo.Shidu = ShiduAddr;

                   
                   
                }
                XArea.Save(AreaPath);
            }
            catch (Exception  ee)
            {

                MessageBox.Show(ee.ToString());
            }
            
          
        }






        //创建xml方法
        public static void CreaNewXml(XmlDocument doc, string rootName)//(，根名称)
        {
            doc.AppendChild(doc.CreateXmlDeclaration("1.0", "UTF-8", null));
            XmlElement root =doc.CreateElement(rootName);
            doc.AppendChild(root);
        
        }

        public static XmlElement Addelement(XmlDocument doc, string nodeName, XmlElement pNode)
        { 
        XmlElement ele=doc.CreateElement(nodeName);
            pNode.AppendChild(ele);
            return ele;
        
        }
        public static void AddElement(XmlDocument doc, string nodeName, XmlElement pNode, string value)
        { 
        XmlElement ele=doc.CreateElement(nodeName);
            ele.InnerText=value;
            pNode.AppendChild(ele);
        
        }

        public static void AddAttribute(XmlDocument doc, string attrName, XmlElement node, string value)
        { 
        XmlAttribute attr= XArea.CreateAttribute(attrName);
            attr.Value=value;
            node.AppendChild(attr);


        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (Seid > 0)
            {
                try
                {
                    string st = listBox1.SelectedItem.ToString();
                    string[] arr = st.Split(' ').Where(str => str != "").ToArray();
                    if (MessageBox.Show("删除区域", $"你确定要删除{arr[2]}区域?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        AreaInfo areaInfo = FrmMain.areasList.Find(s => s.AreaId == int.Parse(arr[0]));
                        if (areaInfo != null)
                        {

                            var root = XArea.DocumentElement;
                            var node = root.SelectSingleNode($"Area[AreaId={int.Parse(arr[0])}]");
                            if (node != null)
                            {
                                FrmMain.areasList.Remove(areaInfo);
                                root.RemoveChild(node);//删除节点
                                MessageBox.Show($"删除{arr[2]}区域成功！");
                                XArea.Save(AreaPath);
                            }

                        }
                    }
                }
                catch (Exception)
                {

                    MessageBox.Show("请选择");
                }
               
                
                

            }
        }

        private static int Seid;
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItems!=null)
            {
                if (listBox1.SelectedItem == null)
                {
                    return;
                }
                string st=listBox1.SelectedItem.ToString();
                string[] arr =st.Split(' ').Where(str=>str!="").ToArray();
                Seid = int.Parse(arr[1]);
                txtAreaId.Text = arr[0];
                txtAreaName.Text = arr[2];
                txtWendu.Text = arr[3];
                txtShidu.Text = arr[4];

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
           
            this.Close();
            
        }
    }
}
